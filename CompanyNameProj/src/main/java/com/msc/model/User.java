package com.msc.model;

import java.sql.Date;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "email"))
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String eId;
	private String firstName;
	private String middleName;
	private String lastName;
	private String email;
	private String dob;
	private int age;
	private String gender;
	private String martialStatus;
	private String bloodGroup;
	private String perAddr;
	private String perAddrTel;
	private String perAddrMob;
	private String preAddr;
	private String preAddrTel;
	private String preAddrMob;
	private String emgAddr;
	private String emgContNo;
	private String emgAContNo;
	private String emgRelation;
	private String password;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "users_roles", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
	private Collection<Role> roles;

	public User() {
	}

	/*
	 * public User(String eId, String firstName, String middleName, String lastName,
	 * String persEmail, String password) { this.eId = eId; this.firstName =
	 * firstName; this.middleName = middleName; this.lastName = lastName;
	 * this.persEmail = persEmail; this.password = password; }
	 * 
	 * public User(String eId, String firstName, String middleName, String lastName,
	 * String persEmail, String password, Collection<Role> roles) { this.eId = eId;
	 * this.firstName = firstName; this.middleName = middleName; this.lastName =
	 * lastName; this.persEmail = persEmail; this.password = password; this.roles =
	 * roles; }
	 */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String geteId() {
		return eId;
	}

	public void seteId(String eId) {
		this.eId = eId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMartialStatus() {
		return martialStatus;
	}

	public void setMartialStatus(String martialStatus) {
		this.martialStatus = martialStatus;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public String getPerAddr() {
		return perAddr;
	}

	public void setPerAddr(String perAddr) {
		this.perAddr = perAddr;
	}

	public String getPerAddrTel() {
		return perAddrTel;
	}

	public void setPerAddrTel(String perAddrTel) {
		this.perAddrTel = perAddrTel;
	}

	public String getPerAddrMob() {
		return perAddrMob;
	}

	public void setPerAddrMob(String perAddrMob) {
		this.perAddrMob = perAddrMob;
	}

	public String getPreAddr() {
		return preAddr;
	}

	public void setPreAddr(String preAddr) {
		this.preAddr = preAddr;
	}

	public String getPreAddrTel() {
		return preAddrTel;
	}

	public void setPreAddrTel(String preAddrTel) {
		this.preAddrTel = preAddrTel;
	}

	public String getPreAddrMob() {
		return preAddrMob;
	}

	public void setPreAddrMob(String preAddrMob) {
		this.preAddrMob = preAddrMob;
	}

	public String getEmgAddr() {
		return emgAddr;
	}

	public void setEmgAddr(String emgAddr) {
		this.emgAddr = emgAddr;
	}

	public String getEmgContNo() {
		return emgContNo;
	}

	public void setEmgContNo(String emgContNo) {
		this.emgContNo = emgContNo;
	}

	public String getEmgAContNo() {
		return emgAContNo;
	}

	public void setEmgAContNo(String emgAContNo) {
		this.emgAContNo = emgAContNo;
	}

	public String getEmgRelation() {
		return emgRelation;
	}

	public void setEmgRelation(String emgRelation) {
		this.emgRelation = emgRelation;
	}

	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", eId=" + eId + ", firstName=" + firstName + ", middleName=" + middleName
				+ ", lastName=" + lastName + ", email=" + email + ", dob=" + dob + ", age=" + age + ", gender=" + gender
				+ ", martialStatus=" + martialStatus + ", bloodGroup=" + bloodGroup + ", perAddr=" + perAddr
				+ ", perAddrTel=" + perAddrTel + ", perAddrMob=" + perAddrMob + ", preAddr=" + preAddr + ", preAddrTel="
				+ preAddrTel + ", preAddrMob=" + preAddrMob + ", emgAddr=" + emgAddr + ", emgContNo=" + emgContNo
				+ ", emgAContNo=" + emgAContNo + ", emgRelation=" + emgRelation + ", password=" + password + ", roles="
				+ roles + "]";
	}

	public User(String eId, String firstName, String middleName, String lastName, String persEmail, String dob,
			String gender, String martialStatus, String bloodGroup, String perAddr, String perAddrTel,
			String perAddrMob, String preAddr, String preAddrTel, String preAddrMob, String emgAddr, String emgContNo,
			String emgAContNo, String emgRelation, String password, Collection<Role> roles) {
		this.eId = eId;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.email = persEmail;
		this.dob = dob;
		this.gender = gender;
		this.martialStatus = martialStatus;
		this.bloodGroup = bloodGroup;
		this.perAddr = perAddr;
		this.perAddrTel = perAddrTel;
		this.perAddrMob = perAddrMob;
		this.preAddr = preAddr;
		this.preAddrTel = preAddrTel;
		this.preAddrMob = preAddrMob;
		this.emgAddr = emgAddr;
		this.emgContNo = emgContNo;
		this.emgAContNo = emgAContNo;
		this.emgRelation = emgRelation;
		this.password = password;
		this.roles = roles;
	}

	public User(String eId, String firstName, String middleName, String lastName, String persEmail, String dob,
			String gender, String martialStatus, String bloodGroup, String perAddr, String perAddrTel,
			String perAddrMob, String preAddr, String preAddrTel, String preAddrMob, String emgAddr, String emgContNo,
			String emgAContNo, String emgRelation, String password) {
		this.eId = eId;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.email = persEmail;
		this.dob = dob;
		this.gender = gender;
		this.martialStatus = martialStatus;
		this.bloodGroup = bloodGroup;
		this.perAddr = perAddr;
		this.perAddrTel = perAddrTel;
		this.perAddrMob = perAddrMob;
		this.preAddr = preAddr;
		this.preAddrTel = preAddrTel;
		this.preAddrMob = preAddrMob;
		this.emgAddr = emgAddr;
		this.emgContNo = emgContNo;
		this.emgAContNo = emgAContNo;
		this.emgRelation = emgRelation;
		this.password = password;
	}
}
