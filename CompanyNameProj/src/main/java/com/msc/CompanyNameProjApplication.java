package com.msc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
//@ComponentScan({"com.msc.*"})
//enable if you want to use Spring XML Configuration
//@ImportResource("classpath:spring-security-config.xml")
public class CompanyNameProjApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompanyNameProjApplication.class, args);
	}

}
