package com.msc.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.msc.model.User;
import com.msc.web.dto.UserRegistrationDto;

public interface UserService extends UserDetailsService {

    User findByEmail(String email);

    User save(UserRegistrationDto registration);
    
    void updatePassword(String password, Long userId);
}