package com.msc.web.dto;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import com.msc.constraint.FieldMatch;

@SuppressWarnings("deprecation")
@FieldMatch.List({
		@FieldMatch(first = "password", second = "confirmPassword", message = "The password fields must match"),
		@FieldMatch(first = "email", second = "confirmEmail", message = "The email fields must match") })
public class UserRegistrationDto implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8316413975453568687L;

	private String eId;

	@NotEmpty
	private String firstName;

	@NotEmpty
	private String middleName;

	@NotEmpty
	private String lastName;

	@NotEmpty
	private String password;

	@NotEmpty
	private String confirmPassword;

	@Email
	@NotEmpty
	private String email;

	@Email
	@NotEmpty
	private String confirmEmail;

	@NotEmpty
	private String dob;

	@NotNull
	private int age;
	@NotEmpty
	private String gender;
	@NotEmpty
	private String martialStatus;
	/*
	 * @NotEmpty private String bloodGroup;
	 * 
	 * @NotEmpty private String perAddr;
	 * 
	 * @NotEmpty
	 * 
	 * @Size(min = 0, max = 10) private String perAddrTel;
	 * 
	 * @NotEmpty
	 * 
	 * @Size(min = 0, max = 10) private String perAddrMob;
	 * 
	 * @NotEmpty private String preAddr;
	 * 
	 * @NotEmpty
	 * 
	 * @Size(min = 0, max = 10) private String preAddrTel;
	 * 
	 * @NotEmpty
	 * 
	 * @Size(min = 0, max = 10) private String preAddrMob;
	 * 
	 * @NotEmpty private String emgAddr;
	 * 
	 * @NotEmpty
	 * 
	 * @Size(min = 0, max = 10) private String emgContNo;
	 * 
	 * @NotEmpty
	 * 
	 * @Size(min = 0, max = 10) private String emgAContNo;
	 * 
	 * @NotEmpty private String emgRelation;
	 */

	@AssertTrue
	private Boolean terms;

	public String geteId() {
		return eId;
	}

	public void seteId(String eId) {
		this.eId = eId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getConfirmEmail() {
		return confirmEmail;
	}

	public void setConfirmEmail(String confirmEmail) {
		this.confirmEmail = confirmEmail;
	}

	public Boolean getTerms() {
		return terms;
	}

	public void setTerms(Boolean terms) {
		this.terms = terms;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMartialStatus() {
		return martialStatus;
	}

	public void setMartialStatus(String martialStatus) {
		this.martialStatus = martialStatus;
	}

	/*
	 * public String getBloodGroup() { return bloodGroup; }
	 * 
	 * public void setBloodGroup(String bloodGroup) { this.bloodGroup = bloodGroup;
	 * }
	 * 
	 * public String getPerAddr() { return perAddr; }
	 * 
	 * public void setPerAddr(String perAddr) { this.perAddr = perAddr; }
	 * 
	 * public String getPerAddrTel() { return perAddrTel; }
	 * 
	 * public void setPerAddrTel(String perAddrTel) { this.perAddrTel = perAddrTel;
	 * }
	 * 
	 * public String getPerAddrMob() { return perAddrMob; }
	 * 
	 * public void setPerAddrMob(String perAddrMob) { this.perAddrMob = perAddrMob;
	 * }
	 * 
	 * public String getPreAddr() { return preAddr; }
	 * 
	 * public void setPreAddr(String preAddr) { this.preAddr = preAddr; }
	 * 
	 * public String getPreAddrTel() { return preAddrTel; }
	 * 
	 * public void setPreAddrTel(String preAddrTel) { this.preAddrTel = preAddrTel;
	 * }
	 * 
	 * public String getPreAddrMob() { return preAddrMob; }
	 * 
	 * public void setPreAddrMob(String preAddrMob) { this.preAddrMob = preAddrMob;
	 * }
	 * 
	 * public String getEmgAddr() { return emgAddr; }
	 * 
	 * public void setEmgAddr(String emgAddr) { this.emgAddr = emgAddr; }
	 * 
	 * public String getEmgContNo() { return emgContNo; }
	 * 
	 * public void setEmgContNo(String emgContNo) { this.emgContNo = emgContNo; }
	 * 
	 * public String getEmgAContNo() { return emgAContNo; }
	 * 
	 * public void setEmgAContNo(String emgAContNo) { this.emgAContNo = emgAContNo;
	 * }
	 * 
	 * public String getEmgRelation() { return emgRelation; }
	 * 
	 * public void setEmgRelation(String emgRelation) { this.emgRelation =
	 * emgRelation; }
	 */

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
