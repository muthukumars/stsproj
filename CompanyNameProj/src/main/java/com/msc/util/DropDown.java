package com.msc.util;

import java.util.SortedMap;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:resources.properties")
@ConfigurationProperties(prefix = "dropdownvalue")
public class DropDown {

	private SortedMap userGenderOptions;
	private SortedMap userMartialOptions;

	public SortedMap getUserGenderOptions() {
		return userGenderOptions;
	}

	public void setUserGenderOptions(SortedMap userGenderOptions) {
		this.userGenderOptions = userGenderOptions;
	}

	public SortedMap getUserMartialOptions() {
		return userMartialOptions;
	}

	public void setUserMartialOptions(SortedMap userMartialOptions) {
		this.userMartialOptions = userMartialOptions;
	}
}